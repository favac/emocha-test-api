// Use this hook to manipulate incoming or outgoing data.
// For more information on hooks see: http://docs.feathersjs.com/api/hooks.html

// eslint-disable-next-line no-unused-vars
module.exports = function(options = {}) {
  return async context => {
    // Get each widtget property code
    let { category_code, color_code, size_code } = context.data
    // The code for new widget will be the code join of widget properties
    context.data.code = category_code + color_code + size_code
    return context
  }
}
