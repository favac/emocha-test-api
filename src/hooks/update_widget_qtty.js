// Use this hook to manipulate incoming or outgoing data.
// For more information on hooks see: http://docs.feathersjs.com/api/hooks.html

// eslint-disable-next-line no-unused-vars
module.exports = function(options = {}) {
  return async context => {
    let { data } = context
    let widgetsService = context.app.service('widgets')
    let updatePromises = []
    let queryPromises = []
    data.forEach(item => {
      queryPromises.push(widgetsService.find({ query: { code: item.code } }))
    })
    let queryResult = await Promise.all(queryPromises)
    console.log('queryResult', queryResult)
    data.forEach((item, index) => {
      let sellData = queryResult[index].data[0].sell
      updatePromises.push(
        widgetsService.patch(
          null,
          { sell: sellData + item.qtty },
          { query: { code: item.code } }
        )
      )
    })
    let updateResult = await Promise.all(updatePromises)
    context.result.widgetsUpdated = updateResult
    return context
  }
}
