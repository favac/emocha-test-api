// Initializes the `orders_widgets` service on path `/orders-widgets`
const createService = require('feathers-nedb');
const createModel = require('../../models/orders_widgets.model');
const hooks = require('./orders_widgets.hooks');

module.exports = function (app) {
  const Model = createModel(app);
  const paginate = app.get('paginate');

  const options = {
    Model,
    paginate
  };

  // Initialize our service with any options it requires
  app.use('/orders-widgets', createService(options));

  // Get our initialized service so that we can register hooks
  const service = app.service('orders-widgets');

  service.hooks(hooks);
};
