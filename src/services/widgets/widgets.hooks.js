const widgetGenerateCode = require('../../hooks/widget_generate_code')
const searchRegex = require('../../hooks/searchRegex')

module.exports = {
  before: {
    all: [],
    find: [searchRegex()],
    get: [],
    create: [widgetGenerateCode()],
    update: [],
    patch: [],
    remove: []
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
}
