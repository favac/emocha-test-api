// Initializes the `widgets` service on path `/widgets`
const createService = require('feathers-nedb');
const createModel = require('../../models/widgets.model');
const hooks = require('./widgets.hooks');

module.exports = function (app) {
  const Model = createModel(app);
  const paginate = app.get('paginate');

  const options = {
    Model,
    paginate
  };

  // Initialize our service with any options it requires
  app.use('/widgets', createService(options));

  // Get our initialized service so that we can register hooks
  const service = app.service('widgets');

  service.hooks(hooks);
};
