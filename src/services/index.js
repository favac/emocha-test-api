const categories = require('./categories/categories.service.js');
const colors = require('./colors/colors.service.js');
const sizes = require('./sizes/sizes.service.js');
const widgets = require('./widgets/widgets.service.js');
const orders = require('./orders/orders.service.js');
const ordersWidgets = require('./orders_widgets/orders_widgets.service.js');
// eslint-disable-next-line no-unused-vars
module.exports = function (app) {
  app.configure(categories);
  app.configure(colors);
  app.configure(sizes);
  app.configure(widgets);
  app.configure(orders);
  app.configure(ordersWidgets);
};
