/**
 * Creates initial data for widgets entities
 */
const createInitialWidgets = async app => {
  try {
    // I want to create the initial list of widgets according to
    // categories, colors and sizes

    // I can consult widget properties at the same time
    let promises = []
    promises.push(app.service('colors').find())
    promises.push(app.service('categories').find())
    promises.push(app.service('sizes').find())

    let promisesResult = await Promise.all(promises)

    let colors = promisesResult[0]
    let categories = promisesResult[1]
    let sizes = promisesResult[2]

    // I can create now the widgets

    // widgets service
    const widgetsService = app.service('widgets')

    // first I delete all already in DB

    await widgetsService.remove(null, { query: {} })

    // then create an array with data for new widgets
    let newWidgets = []
    colors.data.forEach(color => {
      categories.data.forEach(category => {
        sizes.data.forEach(size => {
          let description = `${category.name} ${size.name} ${color.name} Widget`
          let code = `${category.code}${color.code}${size.code}`
          newWidgets.push({
            color_code: color.code,
            category_code: category.code,
            size_code: size.code,
            description,
            code,
            qtty: 10,
            sell: 0,
            icon: category.icon,
            color: color.hex_code,
            price: category.price
          })
        })
      })
    })

    // save new widgets
    let result = await widgetsService.create(newWidgets)
    console.log(`${result.length} widgets created`)
  } catch (error) {
    console.log('Error', error)
  }
}

module.exports = {
  createInitialWidgets
}
