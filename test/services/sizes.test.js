const assert = require('assert');
const app = require('../../src/app');

describe('\'sizes\' service', () => {
  it('registered the service', () => {
    const service = app.service('sizes');

    assert.ok(service, 'Registered the service');
  });
});
