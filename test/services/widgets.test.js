const assert = require('assert');
const app = require('../../src/app');

describe('\'widgets\' service', () => {
  it('registered the service', () => {
    const service = app.service('widgets');

    assert.ok(service, 'Registered the service');
  });
});
