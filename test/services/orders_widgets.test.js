const assert = require('assert');
const app = require('../../src/app');

describe('\'orders_widgets\' service', () => {
  it('registered the service', () => {
    const service = app.service('orders-widgets');

    assert.ok(service, 'Registered the service');
  });
});
