const assert = require('assert');
const feathers = require('@feathersjs/feathers');
const widgetGenerateCode = require('../../src/hooks/widget_generate_code');

describe('\'widget_generate_code\' hook', () => {
  let app;

  beforeEach(() => {
    app = feathers();

    app.use('/dummy', {
      async get(id) {
        return { id };
      }
    });

    app.service('dummy').hooks({
      before: widgetGenerateCode()
    });
  });

  it('runs the hook', async () => {
    const result = await app.service('dummy').get('test');
    
    assert.deepEqual(result, { id: 'test' });
  });
});
